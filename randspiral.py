#THIS IS THE ONE THAT YOU'RE EDITING 27/04/11

from numpy import *
from math import *
import sys
#import re
countries = []
tracks = []
trackfeatures = []
trackneighbours = [] # each entry is an array of that track's neighbours, index is track index in tracks
k=5
longs = []
lats = []
landsea = zeros([180,360]) #lat long terrain

#tracks data from ARFF
def getdata(file):
	global tracks
	features = []
	#the above should be the same length, i.e. each track should have a set of feature-values
	with open(file,'r') as infile:
		for line in infile:
			if "filename" in line:
				index = line.rfind('/')
				trackname = line[(index+1):]
				#print trackname
				tracks.append(trackname)
			elif "," in line:
				#print line
				features.append(line)
		if len(tracks) != len(features):
			sys.exit(2)
	global trackfeatures
	trackfeatures = zeros([len(tracks),len(features[0].split(","))])
	for n, track in enumerate(tracks):
		#print n, track
		featurelist = features[n].split(",")
		for m, feature in enumerate(featurelist):
			#print m,feature
			try:
				value = double(feature)
				trackfeatures[n,m] = value
			except ValueError:
				pass
				
#countries lat longs
def getlatlong(thefile):
	global countries
	for line in open(thefile,'r'):
		bits = line.split(",")
		code = bits[0]
		lat = float(bits[2])
		long = float(bits[3][:-1])
		countries.append([code,lat,long])

def roundNASA(place):
	nasalat = int(place[0]) + 0.5
	nasalong = int(place[1]) + 0.5
	nasacoords = [nasalat,nasalong]
	#print nasacoords
	return nasacoords

#squares = []
def knn():
	alld = [] #distance from true
	alldl = [] #nearest land distance from true
	n = len(trackfeatures) #number of tracks
	m = len(trackfeatures[0]) #number of features per track
	last = len(trackfeatures)-1
	print n, m
	differences = zeros([n,n,m]) #to hold the difference per track-pair-feature
	for firstindex, firstset in enumerate(trackfeatures): # is suboptimal but duplication allows easy indexing
		for secondindex, secondset in enumerate(trackfeatures):
			if firstindex != secondindex:
				diff = zeros([m])
				#compare each feature in turn
				for index, firstvalue in enumerate(firstset):
					secondvalue = secondset[index]
					#print "values are: " + str(firstvalue) +" and "+ str(secondvalue)
					diff[index] = square(firstvalue-secondvalue)
					differences[firstindex,secondindex,index] = diff[index] 
		neighbours = differences[firstindex,:,:]
		#print neighbours
		totaldiffs = []
		#print totaldiffs
		for index, close in enumerate(neighbours):
			#print index
			totaldiffs.append(sqrt(sum(close)))
		if firstindex != 0 & firstindex != last:
			allother = totaldiffs[:firstindex] + totaldiffs[firstindex+1:]
		elif firstindex == 0: #first one has no preceding indices
			allother = totaldiffs[1:]
		elif firstindex == last: #last has no succeeding indices
			allother = totaldiffs[:-1]
		#print str(k)
		if k == 1:
			#print "k = 1"
			x = min(allother)
			xi = allother.index(x)
			if xi >= firstindex: #rejig indices after removal of self
				xi = xi+1
			#print tracks[firstindex] + "is closest to " + tracks[xi] 
			realCoords = [0,0]
			nearestCoords = [0,0]
			done = 0
			for country in countries:
				if country[0] == tracks[firstindex][:2]:
					realCoords = [country[1],country[2]]
					done = done+1
				if country[0] == tracks[xi][:2]:
					nearestCoords = [country[1],country[2]]
					done = done+1
				if done == 2: #stop when both found
					break
			#print realCoords, nearestCoords
			#print d 
			alld.append(d)
			#THIS DOES NOT DO THE SQUARE ROUTE THING
		else:
			#print "k != 1"
			findmin = []
			for item in allother:
				findmin.append(item)
			neighbours = []
			nindices = []
			#print tracks[firstindex][:-5]
			for i in range(k):
				closest = random.choice(findmin) # RANDOM !!!!
				closesti = allother.index(closest)
				if closesti >= firstindex: #rejig indices after removal of self
					closesti = closesti+1
				#print closest, closesti, tracks[closesti]
				
				
				#print tracks[closesti][:4] + " "
				neighbours.append(closest)
				nindices.append(closesti)
				findmin.remove(closest)
			realCoords = [0,0]
			nearestCoords = []
			done = 0
			for country in countries:
				if country[0] == tracks[firstindex][:2]:
					realCoords = [country[1],country[2]]
					done = done+1
				for nn in nindices:
					if country[0] == tracks[nn][:2]:
						nearestCoords.append([country[1],country[2]])
						done = done+1
				if done == (1+k): #stop when both found
					break
			#print realCoords, nearestCoords
			average = avgSphericalCoords(nearestCoords)
			#print realCoords, average
			d = sphericalDistance(realCoords,average)
			alld.append(d)
			nasaReal = roundNASA(realCoords)
			nasaAverage = roundNASA(average)
			####################
			#SPIRAL
			nearestLand = []
			nearestLand.append(nasaAverage[0])
			nearestLand.append(nasaAverage[1])
			
			while whatIsMyTerrain(nearestLand) == 0: #WATER is 0, land and coast are 1,2
				dlat = nearestLand[0] - nasaAverage[0]
				dlong = nearestLand[1] - nasaAverage[1]
				#print nearestLand, dlat, dlong
				if dlong <=0 and abs(dlong) >= abs(dlat): #this one includes the start point, 
														#and the -lat,-long line, 
														#and the +lat-long line (North bias spiral)
					nearestLand[0] = nearestLand[0] + 1
					print "N"
				elif dlat > 0 and abs(dlat) > abs(dlong): #this includes no lines
					nearestLand[1] = nearestLand[1] + 1
					print "E"
				elif dlong > 0 and (abs(dlong) > abs(dlat) or dlong == dlat): #this includes the +lat, +long line
					nearestLand[0] = nearestLand[0] -1
					print "S"
				elif dlat < 0 and (abs(dlat) > abs(dlong) or dlat == -dlong): #this includes the -lat, +long line
					nearestLand[1] = nearestLand[1] -1
					print "W"
				
				#CHECK FOR INTERNATIONAL DATE LINE
			dl = sphericalDistance(realCoords,nearestLand)
			alldl.append(nearestLand)
			print nearestLand
			print nasaAverage
			########################
		#	myRouteSquares = []
			#myRouteSquares.append(nasaReal) #getSquareRoute does this
			#print nasaReal, nasaAverage
			#getSquareRoute(nasaReal,nasaAverage)
		#	global squares
		#	for sq in squares:
		#		myRouteSquares.append(sq)
		#	squares = []
		#	#myRouteSquares.append(nasaAverage) #getSquareRoute does this
		#	terrain = []
		#	perSquareDistances = []
		#	for index, sq in enumerate(myRouteSquares):
		#		#print sq
		#		terrain.append(whatIsMyTerrain(sq))
		#		if sq != nasaAverage:
		#			perSquareDistances.append(sphericalDistance(sq,myRouteSquares[index + 1]))
		#	
		#	avland = 0
		#	avwater = 0
		#	avcoast = 0
		#	for index,dist in enumerate(perSquareDistances):
		#		if terrain[index] == 0:
		#			avwater =  avwater + dist
		#		if terrain[index] == 1:
		#			avland = avland + dist
		#		if terrain[index] == 2:
		#			avcoast = avcoast + dist
		#	nearCap =  myNearestCapital(average)
		#	#print nearCap[0]
		#	nasaNearCap = roundNASA(nearCap[1:])
		#	capdist = sphericalDistance(realCoords,nearCap[1:])
		#	getSquareRoute(nasaReal,nasaNearCap)
		#	myRouteSquares = []
		#	for sq in squares:
		#		myRouteSquares.append(sq)
		#	squares = []
		#	terr = []
		#	perSqDist = []
		#	for index, sq in enumerate(myRouteSquares):
		#		#print sq
		#		terr.append(whatIsMyTerrain(sq))
		#		if sq != nasaNearCap:
		#			perSqDist.append(sphericalDistance(sq,myRouteSquares[index + 1]))
		#	
		#	land = 0
		#	water = 0
		#	coast = 0
		#	for index,dist in enumerate(perSqDist):
		#		if terr[index] == 0:
		#			water =  water + dist
		#		if terr[index] == 1:
		#			land = land + dist
		#		if terr[index] == 2:
		#			coast = coast + dist
			print tracks[firstindex][:2] + "," + str(d)+ ',' + str(dl)

	#print alld
	#
	maxi = max(alld)
	mini = min(alld)
	tot = 0
	for d in alld:
		tot = tot + d
	avg = tot/len(alld)
	print maxi,mini,avg


#gets the NASA data and puts it in a 3D array: [lat,long,terrain]
#0 = water, 1 = land, 2 = coast
def getLandMask():	
	with open("NASALandmask",'r') as infile:
		for line in infile:
			if ':' in line:
				continue
			if "coordinates" in line:
				temp = line.split(',')
				rawlongs = temp[1:] #remove 'coordinates'
				rawlongs[-1] = rawlongs[-1][:-1] # remove final '\n'
				for degree in rawlongs:
					if degree.endswith('W '): #West is negative
						x = float(degree[:-2]) #knock off the 'W '
						x = x * -1 #make negative
					elif degree.endswith('E'): # East is positive
						x = float(degree[:-1]) #knock off the 'E'
					longs.append(x)
			elif 'S' in line or 'N' in line: #it's a particular latitude's per-longitude terrain status, South is negative
				temp = line.split(',')
				lat = 0
				if 'S' in line:
					lat = float(temp[0][:-1]) * -1 #because it's South
				else:
					lat = float(temp[0][:-1])
				lats.append(lat)
				terrain = []
				stringter = temp[1:]
				stringter[-1] = stringter[-1][0:1] #avoiding occasional \n
				for t in stringter:
					terrain.append(int(t)) #now have array of landvalues that is numeric
				#print terrain
				for i, value in enumerate(longs):
					#print terrain[i]
					landsea[lats.index(lat)][i] = terrain[i] 
					#print landsea[lats.index(lat)][i]
				#print landsea[lats.index(lat)]
	#print landsea			
	return landsea

def avgSphericalCoords(arrayOfCoords):
#with thanks to http://www.geomidpoint.com/calculation.html
#Given the values for the first location in the list:
#Lat1, lon1
#Convert lat/lon to Cartesian coordinates for first location.
#X1 = cos(lat1) * cos(lon1)
#Y1 = cos(lat1) * sin(lon1)
#Z1 = sin(lat1)
#Repeat for all remaining locations in the list.
#take average x,y,z
#Convert average x, y, z coordinate to latitude and longitude.
#Lon = atan2(y, x)
#Hyp = sqrt(x * x + y * y)
#Lat = atan2(z, hyp)
#Special case:
#If abs(x) < 10-9 and abs(y) < 10-9 and abs(z) < 10-9 then the geographic midpoint is the center of the earth.
	xArray = []
	yArray = []
	zArray = []
	for pair in arrayOfCoords: #(lat,long)
		lat = radians(pair[0])
		#print lat
		long = radians(pair[1])
		#print long
		x = cos(lat) * cos(long)
		y = cos(lat) * sin(long)
		z = sin(lat)
		xArray.append(x)
		yArray.append(y)
		zArray.append(z)
	avx = average(xArray)
	avy = average(yArray)
	avz = average(zArray)
	#print avx, avy, avz
	longitude = atan2(avy,avx)
	#print "long" + str(longitude)
	hypotenuse = sqrt((avx*avx)+(avy*avy))
	latitude = atan2(avz, hypotenuse)
	return [degrees(latitude),degrees(longitude)]

def sphericalDistance(home,away): # treat coast as land
	if home == away:
		return 0
################
#With thanks to http://www.movable-type.co.uk/scripts/latlong.html
#HAVERSINE in Javascript
#var R = 6371; // km
#var dLat = (lat2-lat1).radians();
#var dLon = (lon2-lon1).radians(); 
#var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
 #       Math.cos(lat1.radians()) * Math.cos(lat2.radians()) * 
 #       Math.sin(dLon/2) * Math.sin(dLon/2); 
#var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
#var d = R * c;
#SPHERICAL LAW OF COSINES IN JAVASCRIPT (Accuracy to ~ 1m)
#var R = 6371; // km
#var d = Math.acos(Math.sin(lat1)*Math.sin(lat2) + 
#                  Math.cos(lat1)*Math.cos(lat2) *
 #                 Math.cos(lon2-lon1)) * R;
	R = 6731 #km
	homelat = radians(home[0])
	homelong = radians(home[1])
	awaylat = radians(away[0])
	awaylong = radians(away[1])
	d = acos(sin(homelat)*(sin(awaylat)) + cos(homelat)*cos(awaylat)*cos(awaylong-homelong))*R
	return d
	
def whatIsMyTerrain(square):
	#print square
	answer = landsea[lats.index(square[0])][longs.index(square[1])]
	#if int(answer) == 1:
	#	print "LAND"
	#if int(answer) == 2:
	#	print "COAST"
	#if int(answer) == 0:
	#	print "WATER"
	return int(answer)
	
#for any lat/long, find the spherically closest capital from the list.
def myNearestCapital(geo):
	#print geo
	nearest = []
	nearestD = 50000 # True maximum is 21000 ish
	for place in countries:
		#print place
		d = sphericalDistance(geo,place[1:])
		#print d
		if d < nearestD:
			nearestD = d
			nearest = place
	#print nearestD
	return nearest
		
	
#use recursion here

#done = 0
def getSquareRoute(a,b):
	head = []
	possSquares = []
	distances = []
	if a != b:
		#print a,b
		dlat = b[0] - a[0]
		dlong = b[1] - a[1]
		#no IDL to worry about if longitude signs are the same, or if they are different but dlong < 180 you cross UTC instead
		if(cmp(a[1],0) == cmp(b[1],0) or (dlong < 180 and dlong > -180)):
			if(dlat > 0): #going NORTH
				if(dlong > 0): #going EAST
					#we're going NE so both lat and long are positive
					#determine the neigbour squares
					possSquares = [[a[0]+1,a[1] ],[a[0],a[1]+1],[a[0]+1,a[1]+1]] #lat up, long up, both up
					#print possSquares
					
				else: #GO WEST, life is peaceful there
					#we're going NW so lat is positive, long negative
					possSquares = [[a[0]+1,a[1] ],[a[0],a[1]-1],[a[0]+1,a[1]-1]] #lat up, long down, latup longdown
					
			else: #going SOUTH
				#print "CANT GO SOUTH YET"
				if(dlong > 0):#going EAST
					#we're going SE so lat -ve long +ve
					possSquares = [[a[0]-1,a[1]],[a[0],a[1]+1],[a[0]-1,a[1]+1]] #lat down, long up, latdown longup
				else: #GO WEST, in the open air
					#We're going SW so lat and long are both negative
					possSquares = [[a[0]-1,a[1]],[a[0],a[1]-1],[a[0]-1,a[1]-1]] # lat down, long down, both down
		else: #WE ARE CROSSING THE INTERNATIONAL DATE LINE
			#print "Not coded yet"
			if(dlat > 0): #NORTH
				if (dlong > 0): #GOING WEST
					#NW IDL
					if a[1] == -175.5: #Actually the point where we cross
						possSquares = [[a[0]+1,a[1] ],[a[0],175.5],[a[0]+1,175.5]]
					else: 
						possSquares = [[a[0]+1,a[1] ],[a[0],a[1]-1],[a[0]+1,a[1]-1]] #lat up, long down, latup longdown
				else:#Going EAST
					#NE IDL
					if a[1] == 175.5:#Actually the point where we cross
						possSquares = [[a[0]+1,a[1] ],[a[0],-175.5],[a[0]+1,-175.5]] #lat up, long up, both up
					else:
						possSquares = [[a[0]+1,a[1] ],[a[0],a[1]+1],[a[0]+1,a[1]+1]] #lat up, long up, both up
			else:#GOING SOUTH
				if(dlong > 0): #GOING WEST
					#SW IDL
					if a[1] == -175.5:#Actually the point where we cross
						possSquares = [[a[0]-1,a[1]],[a[0],175.5],[a[0]-1,175.5]] # lat down, long down, both down
					else:
						possSquares = [[a[0]-1,a[1]],[a[0],a[1]-1],[a[0]-1,a[1]-1]]# lat down, long down, both down
				else:#Going EAST
					#SE IDL
					if a[1] == 175.5:
						possSquares = [[a[0]-1,a[1]],[a[0],-175.5],[a[0]-1,-175.5]] #lat down, long up, latdown longup
					else:
						possSquares = [[a[0]-1,a[1]],[a[0],a[1]+1],[a[0]-1,a[1]+1]] #lat down, long up, latdown longup
		distances.append(sphericalDistance(possSquares[0],b))
		distances.append(sphericalDistance(possSquares[1],b))
		distances.append(sphericalDistance(possSquares[2],b))
		head = possSquares[distances.index(min(distances))] #find the nearest of the squares to the goal
		#print head
		squares.append(head)
		#print "entering recursion"
		tail = getSquareRoute(head,b)
		#print tail
		if(tail):
			for sq in tail:
				squares.append(sq)
		#else: print "a = b - time to go back up"
		#print squares
		#return squares
		#return squares

#Should return the land distance and the sea distance parts of the total distance, as an array [land,sea,total]
def landSeaSphericalDistance(home,away): # treat coast as land
	getLandMask()
################
#With thanks to http://www.movable-type.co.uk/scripts/latlong.html
#HAVERSINE in Javascript
#var R = 6371; // km
#var dLat = (lat2-lat1).radians();
#var dLon = (lon2-lon1).radians(); 
#var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
 #       Math.cos(lat1.radians()) * Math.cos(lat2.radians()) * 
 #       Math.sin(dLon/2) * Math.sin(dLon/2); 
#var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
#var d = R * c;
#SPHERICAL LAW OF COSINES IN JAVASCRIPT (Accuracy to ~ 1m)
#var R = 6371; // km
#var d = Math.acos(Math.sin(lat1)*Math.sin(lat2) + 
#                  Math.cos(lat1)*Math.cos(lat2) *
 #                 Math.cos(lon2-lon1)) * R;
	R = 6731 #km
	homelat = radians(home[0])
	homelong = radians(home[1])
	awaylat = radians(away[0])
	awaylong = radians(away[1])
	d = acos(sin(homelat)*(sin(awaylat)) + cos(homelat)*cos(awaylat)*cos(awaylong-homelong))*R
	return d

def main():
	infile = "world1159.arff" 
	countryfile = "Country.txt"
	try:
		#infile = sys.argv[1]
		global k
		#k = sys.argv[2]
		#k = int(k) #arguments always come in as strings!!
		getdata(infile)
		#countryfile = sys.argv[3]
		getlatlong(countryfile)
		getLandMask()
		knn()
	#except IndexError:
		#usage()
	except IOError:
		print "cannot open", sys.argv[1]


main()

