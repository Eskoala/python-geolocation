rows = []
def getdata(file):
	global rows
	with open(file,'r') as infile:
		for line in infile:
			row = line.split(",")
			#print row
			rows.append([row[0],float(row[1])])
	rows.sort()
	#print rows
			
def percountry():
	country = "NOPE"
	top = -1
	count = 0
	for row in rows:
		if row[0] == country: #same country
			if row[1] > top:
				top = row[1]
			count = count + 1
		else: #new country
			if top != -1: #we have a result of some sort
				print country, top, count
			country = row[0]#reset to new country
			top = row[1]#reset to equal current value
			count = 1#we have added one value
	print country, top, count

f = "normNMdef.txt"
getdata(f)
percountry()