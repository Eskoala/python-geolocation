AF,Afghanistan,34.51,69.13
AL,Albania,41.33,19.80
DZ,Algeria,36.70,3.21
AO,Angola,-8.83,13.33
AR,Argentina,-34.60,-58.38
AM,Armenia,40.18,44.51
AU,Australia,-35.30,149.12
AT,Austria,48.20,16.35
BE,Belgium,50.85,4.35
BZ,Belize,17.25,-88.76
BA,Bosnia-Herzegovina,43.86,18.41
BR,Brazil,-15.75,-47.95
BG,Bulgaria,42.68,23.31
BF,Burkina Faso,12.33,-1.66
KH,Cambodia,11.55,104.91
CM,Cameroon,3.86,11.51
CA,Canada,45.40,-75.66
CV,Cape Verde,14.91,-23.51
CN,China,39.91,116.38
CO,Columbia,4.65,-74.05
CD,Democratic Republic of Congo,-4.31,15.31
CI,Cote D'Ivoire (Ivory Coast),6.85,-5.30
HR,Croatia (Hrvatska),45.80,16.00
CU,Cuba,23.13,-82.38
DK,Denmark,55.71,12.56
EC,Ecuador,-0.15,-78.35
EG,Egypt,30.03,31.21
ET,Ethiopia,9.03,38.74
MK,F.Y.R.O.M (Macedonia),42.00,21.43
FI,Finland,60.16,24.93
FR,France,48.85,2.35
GM,Gambia,13.46,-16.60
GE,Georgia,41.71,44.78
DE,Germany,52.51,13.38
GH,Ghana,5.55,0.25
GR,Greece,38.00,23.71
GN,Guinea,9.51,-13.70
HU,Hungary,47.43,19.25
IS,Iceland,64.13,-21.93
IN,India,28.61,77.20
ID,Indonesia,-6.17,106.82
IR,Iran,35.68,51.41
IQ,Iraq,33.33,44.43
IL,Israel,31.78,35.21
IT,Italy,41.90,12.48
JM,Jamaica,17.98,-76.80
JP,Japan,35.70,139.71
KZ,Kazhakstan,51.16,71.50
KE,Kenya,-1.26,36.80
XK,Kosovo,42.66,21.16
KG,Kyrgyzstan,42.86,74.60
LB,Lebanon,33.90,35.53
LY,Libya,32.86,13.18
LT,Lithuania,54.68,25.31
ML,Mali,12.65,-8.00
MT,Malta,35.88,14.50
MR,Mauritania,18.15,-15.96
MX,Mexico,19.05,-99.36
MA,Morocco,34.03,-6.85
MZ,Mozambique,-25.95,32.58
MM,Myanmar,19.75,96.10
NL,Netherlands,52.31,5.55
NZ,New Zealand,-41.28,174.45
NE,Niger,13.53,2.08
NG,Nigeria,9.06,7.48
PK,Pakistan,33.66,73.16
PE,Peru,-12.04,-77.02
PL,Poland,52.21,21.03
PT,Portugal,38.76,-9.15
PR,Puerto Rico,18.45,-66.10
RO,Romania,44.41,26.10
RU,Russia,55.75,37.61
SA,Saudi Arabia,24.65,46.76
SN,Senegal,14.66,-17.41
RS,Serbia,44.80,20.46
SL,Sierra Leone,8.48,-13.23
SO,Somalia,2.03,45.35
ZA,South Africa,-33.92,18.42
ES,Spain,40.43,-3.70
SD,Sudan,15.63,32.53
SE,Sweden,59.35,18.06
SY,Syria,33.50,36.30
TJ,Tadjikistan,38.55,68.80
TW,Taiwan,23.76,121.00
TZ,Tanzania,-6.17,35.74
TH,Thailand,13.75,100.48
TN,Tunisia,36.83,10.15
TR,Turkey,39.91,32.83
TM,Turkmenistan,7.96,58.33
TV,Tuvalu,-8.51,179.21
UA,Ukraine,50.45,30.50
UK,United Kingdom,52.50,-0.12
US,United States,38.89,-77.03
UY,Uruguay,-34.88,-56.16
UZ,Uzbekistan,41.26,69.21
