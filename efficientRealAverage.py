from numpy import *
from math import *
import sys
#import re
countries = []
tracks = []
trackfeatures = []

k=5
def usage():
	print "specify an arff file as a parameter"

#gets the NASA data and puts it in a 3D array: [lat,long,terrain]
#0 = water, 1 = land, 2 = coast
def getLandMask():	
	with open("NASALandmask",'r') as infile:
		for line in infile:
			if ':' in line:
				continue
			if "coordinates" in line:
				temp = line.split(',')
				rawlongs = temp[1:] #remove 'coordinates'
				rawlongs[-1] = rawlongs[-1][:-1] # remove final '\n'
				for degree in rawlongs:
					if degree.endswith('W '): #West is negative
						x = float(degree[:-2]) #knock off the 'W '
						x = x * -1 #make negative
					elif degree.endswith('E'): # East is positive
						x = float(degree[:-1]) #knock off the 'E'
					longs.append(x)
			elif 'S' in line or 'N' in line: #it's a particular latitude's per-longitude terrain status, South is negative
				temp = line.split(',')
				lat = 0
				if 'S' in line:
					lat = float(temp[0][:-1]) * -1 #because it's South
				else:
					lat = float(temp[0][:-1])
				lats.append(lat)
				terrain = []
				stringter = temp[1:]
				stringter[-1] = stringter[-1][0:1] #avoiding occasional \n
				for t in stringter:
					terrain.append(int(t)) #now have array of landvalues that is numeric
				#print terrain
				for i, value in enumerate(longs):
					#print terrain[i]
					landsea[lats.index(lat)][i] = terrain[i] 
					#print landsea[lats.index(lat)][i]
				#print landsea[lats.index(lat)]
	#print landsea			
	return landsea

def whatIsMyTerrain(lat,long):
	answer = landsea[lats.index(lat)][longs.index(long)]
	if int(answer) == 1:
		print "LAND"
	if int(answer) == 2:
		print "COAST"
	if int(answer) == 0:
		print "WATER"
	return landsea[lats.index(lat)][longs.index(long)] 

#Should return the land distance and the sea distance parts of the total distance, as an array [land,sea,total]
def landSeaDistance(home,away): # treat coast as land
	getLandMask()
################
#With thanks to http://www.movable-type.co.uk/scripts/latlong.html
#HAVERSINE in Javascript
#var R = 6371; // km
#var dLat = (lat2-lat1).radians();
#var dLon = (lon2-lon1).radians(); 
#var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
 #       Math.cos(lat1.radians()) * Math.cos(lat2.radians()) * 
 #       Math.sin(dLon/2) * Math.sin(dLon/2); 
#var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
#var d = R * c;
#SPHERICAL LAW OF COSINES IN JAVASCRIPT (Accuracy to ~ 1m)
#var R = 6371; // km
#var d = Math.acos(Math.sin(lat1)*Math.sin(lat2) + 
#                  Math.cos(lat1)*Math.cos(lat2) *
 #                 Math.cos(lon2-lon1)) * R;
	R = 6731 #km
	homelat = radians(home[0])
	homelong = radians(home[1])
	awaylat = radians(away[0])
	awaylong = radians(away[1])
	d = acos(sin(homelat)*(sin(awaylat)) + cos(homelat)*cos(awaylat)*cos(awaylong-homelong))*R
	return d
	
	
def getdata(file):
	global tracks
	features = []
	#the above should be the same length, i.e. each track should have a set of feature-values
	with open(file,'r') as infile:
		for line in infile:
			if "filename" in line:
				index = line.rfind('/')
				trackname = line[(index+1):]
				#print trackname
				tracks.append(trackname)
			elif "," in line:
				#print line
				features.append(line)
		if len(tracks) != len(features):
			sys.exit(2)
		#removing the underrepresented ones?
	global trackfeatures
	trackfeatures = zeros([len(tracks),len(features[0].split(","))])
	for n, track in enumerate(tracks):
		#print n, track
		featurelist = features[n].split(",")
		for m, feature in enumerate(featurelist):
			#print m,feature
			try:
				value = double(feature)
				trackfeatures[n,m] = value
			except ValueError:
				pass
	print n+1,m+1

def getlatlong(thefile):
	global countries
	for line in open(thefile,'r'):
		bits = line.split(",")
		code = bits[0]
		lat = float(bits[2])
		long = float(bits[3][:-1])
		countries.append([code,lat,long])
	
def knn():
	alld = []
	n = len(trackfeatures) #number of tracks
	m = len(trackfeatures[0]) #number of features per track
	last = len(trackfeatures)-1
	#max size required = 1/2(n^2 - n) - pascal's triangle
	
	length = int(0.5*(n**2 - n))
	print length
	indexing = []
	pair = 0
	differences = zeros([length,m], dtype=float32) #to hold the difference per track-pair-feature
	for firstindex, firstset in enumerate(trackfeatures):
		trackneighbours = [] # each entry is an array of that track's neighbours
		for secondindex, secondset in enumerate(trackfeatures):
			if firstindex < secondindex:
				indexing.append([firstindex,secondindex])
				diff = zeros([m])
				#compare each feature in turn
				for index, firstvalue in enumerate(firstset):
					secondvalue = secondset[index]
					#print "values are: " + str(firstvalue) +" and "+ str(secondvalue)
					diff[index] = square(firstvalue-secondvalue)
					differences[pair] = diff[index] 
		neighbours = []
		
		#print neighbours
		totaldiffs = []
		#print totaldiffs
		for index, close in enumerate(neighbours):
			#print index
			totaldiffs.append(sqrt(sum(close)))
		if firstindex != 0 & firstindex != last:
			allother = totaldiffs[:firstindex] + totaldiffs[firstindex+1:]
		elif firstindex == 0: #first one has no preceding indices
			allother = totaldiffs[1:]
		elif firstindex == last: #last has no succeeding indices
			allother = totaldiffs[:-1]
		#print str(k)
		if k == 1:
			#print "k = 1"
			x = min(allother)
			xi = allother.index(x)
			if xi >= firstindex: #rejig indices after removal of self
				xi = xi+1
			#print tracks[firstindex] + "is closest to " + tracks[xi] 
			realCoords = [0,0]
			nearestCoords = [0,0]
			done = 0
			for country in countries:
				if country[0] == tracks[firstindex][:2]:
					realCoords = [country[1],country[2]]
					done = done+1
				if country[0] == tracks[xi][:2]:
					nearestCoords = [country[1],country[2]]
					done = done+1
				if done == 2: #stop when both found
					break
			#print realCoords, nearestCoords
			d = sphericalDistance(realCoords,nearestCoords)
			print d 
			alld.append(d)
		else:
			#print "k != 1"
			findmin = []
			for item in allother:
				findmin.append(item)
			neighbours = []
			nindices = []
			#print tracks[firstindex][:-5]
			for i in range(k):
				closest = min(findmin)
				closesti = allother.index(closest)
				if closesti >= firstindex: #rejig indices after removal of self
					closesti = closesti+1
				#print closest, closesti, tracks[closesti]
				
				
				#print tracks[closesti][:4] + " "
				neighbours.append(closest)
				nindices.append(closesti)
				findmin.remove(closest)
			realCoords = [0,0]
			nearestCoords = []
			done = 0
			for country in countries:
				if country[0] == tracks[firstindex][:2]:
					realCoords = [country[1],country[2]]
					done = done+1
				for nn in nindices:
					if country[0] == tracks[nn][:2]:
						nearestCoords.append([country[1],country[2]])
						done = done+1
				if done == (1+k): #stop when both found
					break
			#print realCoords, nearestCoords
			average = avgSphericalCoords(nearestCoords)
			#print realCoords, average
			d = sphericalDistance(realCoords, average)
			print tracks[firstindex][:2] + "," + str(d) + "," + str(average)
			alld.append(d)
	#print alld
	#
	maxi = max(alld)
	mini = min(alld)
	tot = 0
	for d in alld:
		tot = tot + d
	avg = tot/len(alld)
	print maxi,mini,avg

def sphericalDistance(true,musical):
################
#With thanks to http://www.movable-type.co.uk/scripts/latlong.html
#HAVERSINE in Javascript
#var R = 6371; // km
#var dLat = (lat2-lat1).radians();
#var dLon = (lon2-lon1).radians(); 
#var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
 #       Math.cos(lat1.radians()) * Math.cos(lat2.radians()) * 
 #       Math.sin(dLon/2) * Math.sin(dLon/2); 
#var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
#var d = R * c;
#SPHERICAL LAW OF COSINES IN JAVASCRIPT (Accuracy to ~ 1m)
#var R = 6371; // km
#var d = Math.acos(Math.sin(lat1)*Math.sin(lat2) + 
#                  Math.cos(lat1)*Math.cos(lat2) *
 #                 Math.cos(lon2-lon1)) * R;
	R = 6731 #km
	truelat = radians(true[0])
	truelong = radians(true[1])
	muslat = radians(musical[0])
	muslong = radians(musical[1])
	d = acos(sin(truelat)*(sin(muslat)) + cos(truelat)*cos(muslat)*cos(muslong-truelong))*R
	return d # max should be 21146 km, min 0

#############################################
#Should we add weightings for nearestness? - how near is it musically?
#############################################
def avgSphericalCoords(arrayOfCoords):
#with thanks to http://www.geomidpoint.com/calculation.html
#Given the values for the first location in the list:
#Lat1, lon1
#Convert lat/lon to Cartesian coordinates for first location.
#X1 = cos(lat1) * cos(lon1)
#Y1 = cos(lat1) * sin(lon1)
#Z1 = sin(lat1)
#Repeat for all remaining locations in the list.
#take average x,y,z
#Convert average x, y, z coordinate to latitude and longitude.
#Lon = atan2(y, x)
#Hyp = sqrt(x * x + y * y)
#Lat = atan2(z, hyp)
#Special case:
#If abs(x) < 10-9 and abs(y) < 10-9 and abs(z) < 10-9 then the geographic midpoint is the center of the earth.
	xArray = []
	yArray = []
	zArray = []
	for pair in arrayOfCoords: #(lat,long)
		lat = radians(pair[0])
		#print lat
		long = radians(pair[1])
		#print long
		x = cos(lat) * cos(long)
		y = cos(lat) * sin(long)
		z = sin(lat)
		xArray.append(x)
		yArray.append(y)
		zArray.append(z)
	avx = average(xArray)
	avy = average(yArray)
	avz = average(zArray)
	#print avx, avy, avz
	longitude = atan2(avy,avx)
	#print "long" + str(longitude)
	hypotenuse = sqrt((avx*avx)+(avy*avy))
	latitude = atan2(avz, hypotenuse)
	return [degrees(latitude),degrees(longitude)]

longs = []
lats = []
landsea = zeros([180,360]) #lat long terrain


	
	
def main():
	infile = "world1159all.arff" 
	countryfile = "Country.txt"
	try:
		#infile = sys.argv[1]
		global k
		#k = sys.argv[2]
		#k = int(k) #arguments always come in as strings!!
		getdata(infile)
		#countryfile = sys.argv[3]
		getlatlong(countryfile)
		knn()
	#except IndexError:
		#usage()
	except IOError:
		print "cannot open", sys.argv[1]


main()


