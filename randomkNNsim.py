from numpy import *
from math import *
import sys
import random
#NO NEED TO NORMALISE FEATURES - WE HAVE NONE
#import re
countries = []
tracks = []
trackfeatures = []
trackneighbours = [] # each entry is an array of that track's neighbours, index is track index in tracks
k=10
def usage():
	print "specify an arff file as a parameter"

def getdata(file):
	global tracks
	features = []
	#the above should be the same length, i.e. each track should have a set of feature-values
	with open(file,'r') as infile:
		for line in infile:
			if "filename" in line:
				index = line.rfind('/')
				trackname = line[(index+1):]
		
				tracks.append(trackname)
			#elif "," in line:
		
			#	features.append(line)
		#if len(tracks) != len(features):
		#	sys.exit(2)
	#global trackfeatures
	#trackfeatures = zeros([len(tracks),len(features[0].split(","))])
	#for n, track in enumerate(tracks):
		#print n, track
		#featurelist = features[n].split(",")
		#for m, feature in enumerate(featurelist):
			#print m,feature
		#	try:
		#		value = double(feature)
		#		trackfeatures[n,m] = value
		#	except ValueError:
		#		pass

def getlatlong(thefile):
	global countries
	for line in open(thefile,'r'):
		bits = line.split(",")
		code = bits[0]
		lat = float(bits[2])
		long = float(bits[3][:-1])
		countries.append([code,lat,long])
	
def knn():
	alld = []
	n = len(tracks) #number of tracks
	#m = len(trackfeatures[0]) #number of features per track
	last = len(tracks)-1
	#differences = zeros([n,n,m]) #to hold the difference per track-pair-feature
	#for firstindex, firstset in enumerate(trackfeatures): # is suboptimal but duplication allows easy indexing
	#	for secondindex, secondset in enumerate(trackfeatures):
	#		if firstindex != secondindex:
	#			diff = zeros([m])
				#compare each feature in turn
	#			for index, firstvalue in enumerate(firstset):
	#				secondvalue = secondset[index]
					#print "values are: " + str(firstvalue) +" and "+ str(secondvalue)
	#				diff[index] = square(firstvalue-secondvalue)
	#				differences[firstindex,secondindex,index] = diff[index] 
	#	neighbours = differences[firstindex,:,:]
		#print neighbours
	#	totaldiffs = []
		#print totaldiffs
	#	for index, close in enumerate(neighbours):
			#print index
	#		totaldiffs.append(sqrt(sum(close)))
	#	if firstindex != 0 & firstindex != last:
	#		allother = totaldiffs[:firstindex] + totaldiffs[firstindex+1:]
	#	elif firstindex == 0: #first one has no preceding indices
	#		allother = totaldiffs[1:]
	#	elif firstindex == last: #last has no succeeding indices
	#		allother = totaldiffs[:-1]
		#print str(k)
	
	for index, track in enumerate(tracks):
		othertracks = []
		#print track
		for other in tracks:
			if other != track:
				othertracks.append(other)
		#print len(othertracks)
		if k == 1:
			#print "k = 1"			
			x = random.choice(othertracks) #RANDOM
			xi = othertracks.index(x)
			if xi >= index: #rejig indices after removal of self
				xi = xi+1
			#print tracks[firstindex] + "is closest to " + tracks[xi] 
			realCoords = [0,0]
			nearestCoords = [0,0]
			done = 0
			for country in countries:
				if country[0] == tracks[index][:2]:
					realCoords = [country[1],country[2]]
					done = done+1
				if country[0] == tracks[xi][:2]:
					nearestCoords = [country[1],country[2]]
					done = done+1
				if done == 2: #stop when both found
					break
			#print realCoords, nearestCoords
			d = sphericalDistance(realCoords,nearestCoords)
			#print d 
			alld.append(d)
		else:
			#print "k != 1"
			findmin = []
			for item in othertracks:
				findmin.append(item)
			neighbours = []
			nindices = []
			#print tracks[firstindex][:-5]
			for i in range(k):
				closest =random.choice(findmin)
				closesti = othertracks.index(closest) #NOT findmin
				if closesti >= index: #rejig indices after removal of self
					closesti = closesti+1
				neighbours.append(closest)
				nindices.append(closesti)
				findmin.remove(closest)
			realCoords = [0,0]
			nearestCoords = []
			done = 0
			for country in countries:
				if country[0] == tracks[index][:2]:
					realCoords = [country[1],country[2]]
					done = done+1
				for nn in nindices:
					if country[0] == tracks[nn][:2]:
						nearestCoords.append([country[1],country[2]])
						done = done+1
				if done == (1+k): #stop when both found
					break
			#print realCoords, nearestCoords
			average = avgSphericalCoords(nearestCoords)
			#print realCoords, average
			d = sphericalDistance(realCoords, average)
			#print tracks[index][:2] + "," + str(d)
			alld.append(d)
	#print alld
	#
	maxi = max(alld)
	mini = min(alld)
	tot = 0
	for d in alld:
		tot = tot + d
	avg = mean(alld)
	med = median(alld)
	#print maxi
	#print mini
	print med, ",", avg

def sphericalDistance(true,musical):
################
#With thanks to http://www.movable-type.co.uk/scripts/latlong.html
#HAVERSINE in Javascript
#var R = 6371; // km
#var dLat = (lat2-lat1).radians();
#var dLon = (lon2-lon1).radians(); 
#var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
 #       Math.cos(lat1.radians()) * Math.cos(lat2.radians()) * 
 #       Math.sin(dLon/2) * Math.sin(dLon/2); 
#var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
#var d = R * c;
#SPHERICAL LAW OF COSINES IN JAVASCRIPT (Accuracy to ~ 1m)
#var R = 6371; // km
#var d = Math.acos(Math.sin(lat1)*Math.sin(lat2) + 
#                  Math.cos(lat1)*Math.cos(lat2) *
 #                 Math.cos(lon2-lon1)) * R;
	R = 6731 #km
	truelat = radians(true[0])
	truelong = radians(true[1])
	muslat = radians(musical[0])
	muslong = radians(musical[1])
	d = acos(sin(truelat)*(sin(muslat)) + cos(truelat)*cos(muslat)*cos(muslong-truelong))*R
	return d # max should be 21146 km, min 0

#############################################
#Should we add weightings for nearestness? - how near is it musically?
#############################################
def avgSphericalCoords(arrayOfCoords):
#with thanks to http://www.geomidpoint.com/calculation.html
#Given the values for the first location in the list:
#Lat1, lon1
#Convert lat/lon to Cartesian coordinates for first location.
#X1 = cos(lat1) * cos(lon1)
#Y1 = cos(lat1) * sin(lon1)
#Z1 = sin(lat1)
#Repeat for all remaining locations in the list.
#take average x,y,z
#Convert average x, y, z coordinate to latitude and longitude.
#Lon = atan2(y, x)
#Hyp = sqrt(x * x + y * y)
#Lat = atan2(z, hyp)
#Special case:
#If abs(x) < 10-9 and abs(y) < 10-9 and abs(z) < 10-9 then the geographic midpoint is the center of the earth.
	xArray = []
	yArray = []
	zArray = []
	for pair in arrayOfCoords: #(lat,long)
		lat = radians(pair[0])
		#print lat
		long = radians(pair[1])
		#print long
		x = cos(lat) * cos(long)
		y = cos(lat) * sin(long)
		z = sin(lat)
		xArray.append(x)
		yArray.append(y)
		zArray.append(z)
	avx = average(xArray)
	avy = average(yArray)
	avz = average(zArray)
	#print avx, avy, avz
	longitude = atan2(avy,avx)
	#print "long" + str(longitude)
	hypotenuse = sqrt((avx*avx)+(avy*avy))
	latitude = atan2(avz, hypotenuse)
	return [degrees(latitude),degrees(longitude)]

def main():
	infile = "NMdef.arff" 
	countryfile = "Country.txt"
	try:
		#infile = sys.argv[1]
		global k
		#k = sys.argv[2]
		#k = int(k) #arguments always come in as strings!!
		getdata(infile)
		#countryfile = sys.argv[3]
		getlatlong(countryfile)
		for i in range(1000):
			#print i
			knn()
		
	#except IndexError:
		#usage()
	except IOError:
		print "cannot open", sys.argv[1]

main()


