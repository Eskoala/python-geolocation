import geo_helper

def llh_to_complex(lat, long, height):
coords = geo_helper.turn_llh_into_xyz(lat,long,height,'wgs84')
x=coords[0]
y=coords[1]
z=coords[2]
if(z>=0):
    zetaxi = complex(x,y)/1-z
else:
    zetaxi = complex(x,-y)/1+z
return zetaxi
    


