AF,Afghanistan,34.51,69.13
AL,Albania,41.33,19.80
DZ,Algeria,36.70,3.21
AO,Angola,-8.83,13.33
AR,Argentina,-34.60,-58.38
AM,Armenia,40.18,44.51
AU,Australia,-35.30,149.12
AT,Austria,48.20,16.35
BE,Belgium,50.85,4.35
BZ,Belize,17.25,-88.76
BA,Bosnia-Herzegovina,43.86,18.41
BR,Brazil,-15.75,-47.95
BG,Bulgaria,42.68,23.31
BF,Burkina Faso,12.33,-1.66
KH,Cambodia,11.55,104.91
CM,Cameroon,3.86,11.51
CA,Canada,45.40,-75.66
CV,Cape Verde,14.91,-23.51
CN,China,39.91,116.38
CO,Columbia,4.65,-74.05
CD,Democratic Republic of Congo,-4.31,15.31
CI,Cote D'Ivoire (Ivory Coast),6.85,-5.30
HR,Croatia (Hrvatska),45.80,16.00
CU,Cuba,23.13,-82.38
DK,Denmark,55.71,12.56
EC,Ecuador,-0.15,-78.35
EG,Egypt,30.03,31.21
ET,Ethiopia,9.03,38.74
MK,F.Y.R.O.M (Macedonia),42.00,21.43
FI,Finland,60.16,24.93
FR,France,48.85,2.35
GM,Gambia,13.46,-16.60
GE,Georgia,41.71,44.78
DE,Germany,
GH,Ghana,
GR,Greece,
GN,Guinea,
HU,Hungary,
IS,Iceland,
IN,India,
ID,Indonesia,
IR,Iran,
IQ,Iraq,
IL,Israel,
IT,Italy,
JM,Jamaica,
JP,Japan,
KZ,Kazhakstan,
KE,Kenya,
XK,Kosovo,
KG,Kyrgyzstan,
LB,Lebanon,
LY,Libya,
LT,Lithuania,
ML,Mali,
MT,Malta,
MR,Mauritania,
MX,Mexico,
MA,Morocco,
MZ,Mozambique,
MM,Myanmar,
NL,Netherlands,
NZ,New Zealand,
NE,Niger,
NG,Nigeria,
PK,Pakistan,
PE,Peru,
PL,Poland,
PT,Portugal,
PR,Puerto Rico,
RO,Romania,
RU,Russia,
SA,Saudi Arabia,
SN,Senegal,
RS,Serbia,
SL,Sierra Leone,
SO,Somalia,
ZA,South Africa,
ES,Spain,
SD,Sudan,
SE,Sweden,
SY,Syria,
TJ,Tadjikistan,
TW,Taiwan,
TZ,Tanzania,
TH,Thailand,
TN,Tunisia,
TR,Turkey,
TM,Turkmenistan,
TV,Tuvalu,
UA,Ukraine,48.808076188342,31.766935926448
UK,United Kingdom,52.745166767654,-1.6847761296012
US,United States,37.52,92.17,2010
UY,Uruguay,
UZ,Uzbekistan,


Based on 2000 population
countrynm                     latitude                      longitude                     population
Albania                       41.174529494701               19.929275580053               3234261
Andorra                       42.513020857777               1.5595988866019               105395
Austria                       47.765386201318               14.645625300333               8041532
Belarus                       53.531624124024               27.847175354981               9983978
Belgium                       50.844005826061               4.4332869095216               10296537
Bosnia-Herzegovina            44.160791721547               17.753208075376               4208793
Bulgaria                      42.754116369708               25.083976957381               7569591
Croatia                       45.317637428417               16.262950671815               4660502
Cyprus                        34.971256769794               33.263699125182               814579
Czech Republic                49.821456149539               15.617527756779               10218427
Denmark                       55.853326754724               10.856715208377               5362167
Estonia                       58.957945858648               25.572740786761               1316257
Faeroe Islands                62.094658556062               -6.8992666870599              48516
Finland                       61.755732589277               24.98467066628                5189307
France                        47.143228746162               2.6764463428893               60302304
Germany                       50.855573924694               9.6963409646128               81860002
Gibraltar                     36.136024475098               -5.3476929664612              26409
Greece                        38.686808689502               23.323965300494               10630501
Guernsey                      49.484886169434               -2.5164697170258              56253
Holy See                      41.831249237061               12.541416168213               780
Hungary                       47.288770753717               19.388772968949               9720572
Iceland                       64.372216876845               -21.045029641756              288804
Ireland                       53.111585555903               -7.4282382442794              3989698
Isle of Man                   54.185676063553               -4.5305895148935              78562
Italy                         42.870086858764               12.12890612484                57165124
Jersey                        49.219615936279               -2.1376717090607              88459
Latvia                        56.831191706188               24.496056054831               2353320
Liechtenstein                 47.152644353309               9.5490772442771               34418
Lithuania                     55.223194780885               23.887086150639               3652724
Luxembourg                    49.643734947502               6.0837996175026               463708
Macedonia                     41.742844591767               21.554126089671               2064147
Malta                         35.898859068436               14.457981961126               397732
Monaco                        43.740207672119               7.4421668052673               34927
Netherlands                   52.072871145825               5.2875541627667               16142345
Norway                        61.128336570352               9.9468336009803               4551686
Poland                        51.707976823759               19.308388806995               38426947
Portugal                      39.74693753116                -9.1672490596965              10080168
Republic of Moldova           47.174628112514               28.599711045173               4239195
Romania                       45.692835166704               25.283411622442               22150243
Russia                        53.81563056044                54.539619724253               140919874
San Marino                    43.93830871582                12.463205337524               28032
Serbia and Montenegro         43.72573916897                20.74644037035                10705472
Slovakia                      48.662536038829               19.164300350224               5419247
Slovenia                      46.169295822703               14.89236963709                1975861
Spain                         39.720397339383               -3.2923251997811              39874345
Svalbard                      78.597373962402               15.905464172363               2411
Sweden                        58.913317696441               15.528746561364               8784967
Switzerland                   47.025712614417               7.9586515381984               7148011
Ukraine                       48.808076188342               31.766935926448               47298326
United Kingdom                52.745166767654               -1.6847761296012              59940190