# Create a set of pairs of track which maximises the distance between each pair on average
# and contains no co-located pairs.
#A: read in country locations.
#B: read in track names.
#C: associate a with b
#D: find all possible track pairs
#E: find all possible pair sets that do not contain any co-located tracks
#(is this find all pair sets, then remove bad sets, or remove bad pairs, find remaining sets?)
#F: find the optimum mean distance between each pair for the surviving pair sets
#G: return the winning set.

countries = {}
geotracks = {}
tracks = set()
allpairs = set() #never from the same country

#A
def getlatlong(f):
	global countries
	for line in open(f,'r'):
		parts = line.split(",")
		code = parts[0]
		lat = float(parts[2])
		long = float(parts[3][:-1])
		countries[code] =[lat,long]

		
#B,C
def getdata(f):
	geotrack = []
	for line in open(f,'r'):
		tracks.add(line[:-1]) # not sure yet which will be more use - straight array or hash
		geotracks[line[:-1]] = countries[line[6:8]] # hashed to the lat and long

#D,E
def makeallnoncolocatedpairs():
	global allpairs
	global tracks
	for t1 in tracks:
		for t2 in tracks:
			#check they aren't from the same country
			if (geotracks[t1] != geotracks[t2]):
				allpairs.add([t1,t2])
	print size(allpairs)


#E
def getallsets():
	n = (len(tracks)/2).floor()
	print n
	allpossiblesets = set()
	#A set is made of all the tracks in pairs with no overlap.
	#If there are an odd number of tracks there will be one unpaired track per set!
		#remove all pairs that are not distinct from the set at large
	thisset = set()
	while(len(thisset) < n)
		if distinctpairs(pair, poss):
		
		
#F,G
#def getbestset():
	
def distinctpairs(p1,p2):
	if p1[0] not in p2 and p1[1] not in p2:
		return 1
	return 0

def main():
	songs = "justwavs.txt"
	countries = "Country.txt"
	try:
		getlatlong(countries)
		getdata(songs)
		makeallnoncolocatedpairs()
	except IOError:
		print "cannot open", sys.argv[1]

main()
