from numpy import *
import sys
#import re
tracks = []
trackfeatures = []
#tracklocations = [] # not used
def usage():
	print "specify a .arff file as a parameter"
	
#def torad(degrees):
#	return degrees * (math.pi / 180)

def getdata(file):
	global tracks
	features = []
	#the above should be the same length, i.e. each track should have a set of feature-values
	with open(file,'r') as infile:
		for line in infile:
			if "filename" in line:
				index = line.rfind('/')
				trackname = line[(index+1):]
				#print trackname
				tracks.append(trackname)
			elif "," in line:
				#print line
				features.append(line)
		if len(tracks) != len(features):
			sys.exit(2)
	global trackfeatures
	trackfeatures = zeros([len(tracks),len(features[0].split(","))])
	for n, track in enumerate(tracks):
		#print n, track
		featurelist = features[n].split(",")
		for m, feature in enumerate(featurelist):
			#print m,feature
			try:
				value = double(feature)
				trackfeatures[n,m] = value
			except ValueError:
				pass
	#print trackfeatures
#Something has screwed up here!!!! The results have changed!!
def knn():
	print "MUSICAL DISTANCES"
	n = len(trackfeatures) #number of tracks
	m = len(trackfeatures[0]) #number of features per track
	last = len(trackfeatures)-1
	differences = zeros([n,n,m]) #to hold the difference per track-pair-feature
	for firstindex, firstset in enumerate(trackfeatures): # is suboptimal but duplication allows easy indexing
		for secondindex, secondset in enumerate(trackfeatures): 
			if firstindex != secondindex:
				diff = zeros([m])
				#compare each feature in turn
				for index, firstvalue in enumerate(firstset):
					secondvalue = secondset[index]
					#print "values are: " + str(firstvalue) +" and "+ str(secondvalue)
					diff[index] = square(firstvalue-secondvalue)
					differences[firstindex,secondindex,index] = diff[index] 
		neighbours = differences[firstindex,:,:]
		#print neighbours
		totaldiffs = []
		#print totaldiffs
		for index, close in enumerate(neighbours):
			#print index
			totaldiffs.append(sqrt(sum(close)))
		if firstindex != 0 & firstindex != last:
			allother = totaldiffs[:firstindex] + totaldiffs[firstindex+1:]
		elif firstindex == 0: #first one has no preceding indices
			allother = totaldiffs[1:]
		elif firstindex == last: #last has no succeeding indices
			allother = totaldiffs[:-1]
		#print str(k)
		if k == 1:
			#print "k = 1"
			x = min(allother)
			xi = allother.index(x)
			if xi >= firstindex: #rejig indices after removal of self
				xi = xi+1
			print tracks[firstindex] + "is closest to " + tracks[xi] 
		else:
			#print "k != 1"
			findmin = []
			for item in allother:
				findmin.append(item)
			neighbours = []
			nindices = []
			print tracks[firstindex][:-5]
			for i in range(k):
				closest = min(findmin)
				closesti = allother.index(closest)
				if closesti >= firstindex: #rejig indices after removal of self
					closesti = closesti+1
				#print closest, closesti, tracks[closesti]
				print tracks[closesti][:4] + " "
				neighbours.append(closest)
				nindices.append(closesti)
				findmin.remove(closest)



def main():
	infile = "world.arff" 
	countryfile = "Country.txt"
	try:
		infile = sys.argv[1]
		global k
		k = sys.argv[2]
		k = int(k) #arguments always come in as strings!!
		getdata(infile)
		knn()
		#realNN(countryfile)
	except IndexError:
		usage()
	except IOError:
		print "cannot open", sys.argv[1]


main()


