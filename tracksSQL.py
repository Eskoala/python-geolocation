tracks = []

def readtracks(f):
	global tracks
	for line in open(f,'r'):
		tracks.append(line[:-1])

def makeSQL():
	global tracks
	start = "INSERT INTO Track (filename) VALUES ('"
	allSQL = ""
	end = "');"
	for track in tracks:
		allSQL = allSQL + start + track + end
	return allSQL

def main():
	file = "justwavs.txt"
	try:
		readtracks(file)
		print makeSQL()
	except IOError:
		print "cannot open", sys.argv[1]

main()