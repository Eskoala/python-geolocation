*     ZGELSD Example Program Text
*     NAG Copyright 2005.
*     .. Parameters ..
*    $                 LRWORK=1
      INTEGER          NIN, NOUT
      PARAMETER        (NIN=5,NOUT=6)
      INTEGER          MMAX, NB, NLVL, NMAX, SMLSIZ
      PARAMETER        (MMAX=8,NB=64,NLVL=10,NMAX=16,SMLSIZ=25)
      INTEGER          LDA, LIWORK, LRWORK, LWORK
      PARAMETER        (LDA=MMAX,LIWORK=3*MMAX*NLVL+11*MMAX,
     +                 LRWORK=10*MMAX+2*MMAX*SMLSIZ+8*MMAX*NLVL+3*
     +                 SMLSIZ+(SMLSIZ+1)**2,LWORK=2*MMAX+NB*(MMAX+NMAX))
*     .. Local Scalars ..
      DOUBLE PRECISION RCOND
      INTEGER          I, INFO, J, M, N, RANK
*     .. Local Arrays ..
      COMPLEX *16      A(LDA,NMAX), B(NMAX), WORK(LWORK)
      DOUBLE PRECISION RWORK(LRWORK), S(MMAX)
      INTEGER          IWORK(LIWORK)
*     .. External Subroutines ..
      EXTERNAL         ZGELSD
*     .. Executable Statements ..
      WRITE (NOUT,*) 'ZGELSD Example Program Results'
      WRITE (NOUT,*)
*     Skip heading in data file
      READ (NIN,*)
      READ (NIN,*) M, N
      IF (M.LE.MMAX .AND. N.LE.NMAX .AND. M.LE.N) THEN
*
*        Read A and B from data file
*
         READ (NIN,*) ((A(I,J),J=1,N),I=1,M)
         READ (NIN,*) (B(I),I=1,M)
*
*        Choose RCOND to reflect the relative accuracy of the input
*        data
*
         RCOND = 0.01D0
*
*        Solve the least squares problem min( norm2(b - Ax) ) for the
*        x of minimum norm.
*
         CALL ZGELSD(M,N,1,A,LDA,B,N,S,RCOND,RANK,WORK,LWORK,RWORK,
     +               IWORK,INFO)
*
         IF (INFO.EQ.0) THEN
*
*           Print solution
*
            WRITE (NOUT,*) 'Least squares solution'
            WRITE (NOUT,99999) (B(I),I=1,N)
*
*           Print the effective rank of A
*
            WRITE (NOUT,*)
            WRITE (NOUT,*) 'Tolerance used to estimate the rank of A'
            WRITE (NOUT,99998) RCOND
            WRITE (NOUT,*) 'Estimated rank of A'
            WRITE (NOUT,99997) RANK
*
*           Print singular values of A
*
            WRITE (NOUT,*)
            WRITE (NOUT,*) 'Singular values of A'
            WRITE (NOUT,99996) (S(I),I=1,M)
         ELSE IF (INFO.GT.0) THEN
            WRITE (NOUT,*) 'The SVD algorithm failed to converge'
         END IF
      ELSE
         WRITE (NOUT,*) 'MMAX and/or NMAX too small, and/or M.GT.N'
      END IF
      STOP
*
99999 FORMAT (4(' (',F7.4,',',F7.4,')',:))
99998 FORMAT (3X,1P,E11.2)
99997 FORMAT (1X,I6)
99996 FORMAT (1X,7F11.4)
      END