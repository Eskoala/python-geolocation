from numpy import *
import sys
tracks = []
trackfeatures = []
def usage():
	print "specify a .arff file as a parameter"

def getdata(file):
	global tracks
	features = []
	#the above should be the same length, i.e. each track should have a set of feature-values
	with open(file,'r') as infile:
		for line in infile:
			if "filename" in line:
				index = line.rfind('/')
				trackname = line[(index+1):]
				#print trackname
				tracks.append(trackname)
			elif "," in line:
				#print line
				features.append(line)
		if len(tracks) != len(features):
			sys.exit(2)
	global trackfeatures
	trackfeatures = zeros([len(tracks),len(features[0].split(","))])
	for n, track in enumerate(tracks): 
		#print n, track
		featurelist = features[n].split(",")
		for m, feature in enumerate(featurelist):
			#print m,feature
			try:
				value = double(feature)
				trackfeatures[n,m] = value
			except ValueError:
				pass
	#print trackfeatures

def knn():
	n = len(trackfeatures) #number of tracks
	m = len(trackfeatures[0]) #number of features per track
	last = len(trackfeatures)-1
	differences = zeros([n,n,m]) #to hold the difference per track-pair-feature
	for firstindex, firstset in enumerate(trackfeatures): # is suboptimal but duplication allows easy indexing
		for secondindex, secondset in enumerate(trackfeatures): 
			if firstindex != secondindex:
				diff = zeros([m])
				#compare each feature in turn
				for index, firstvalue in enumerate(firstset):
					secondvalue = secondset[index]
					#print "values are: " + str(firstvalue) +" and "+ str(secondvalue)
					diff[index] = square(firstvalue-secondvalue)
					differences[firstindex,secondindex,index] = diff[index] 
		neighbours = differences[firstindex,:,:]
		#print neighbours
		totaldiffs = []
		#print totaldiffs
		for index, close in enumerate(neighbours):
			#print index
			totaldiffs.append(sqrt(sum(close)))
		if firstindex != 0 & firstindex != last:
			allother = totaldiffs[:firstindex] + totaldiffs[firstindex+1:]
		elif firstindex == 0: #first one has no preceding indices
			allother = totaldiffs[1:]
		elif firstindex == last: #last has no succeeding indices
			allother = totaldiffs[:-1]
		print str(k)
		if k == 1:
			print "k = 1"
			x = min(allother)
			xi = allother.index(x)
			if xi >= firstindex: #rejig indices after removal of self
				xi = xi+1
			print tracks[firstindex] + "is closest to " + tracks[xi] 
		else:
			print "k!=1"
		#######
		#TODO: increase k
		#Thoughts: make final array 2d to include track id, then sort
		#OR: search through for the lowest k values - one pass should be quicker than a sort.
		#######

def main():
	infile = "world.arff" 
	try:
		infile = sys.argv[1]
		global k
		k = sys.argv[2]
		k = int(k)
		getdata(infile)
		knn()
	except IndexError:
		usage()
	except IOError:
		print "cannot open", sys.argv[1]


main()


