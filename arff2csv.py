#tracks data from ARFF
def getdata(file):
	global tracks
	features = []
	#the above should be the same length, i.e. each track should have a set of feature-values
	with open(file,'r') as infile:
		for line in infile:
			if "filename" in line:
				index = line.rfind('/')
				country = line[index+1:index+3]
				trackname = line[(index+1):]
				print country
			elif "," in line:
				#print line
				features.append(line)
		if len(tracks) != len(features):
			print("arrays not equal")
			sys.exit(2)
	global trackfeatures
	trackfeatures = zeros([len(tracks),len(features[0].split(","))-1])
	for n, track in enumerate(tracks):
		#print n, track
		featurelist = features[n].split(",")
		for m, feature in enumerate(featurelist):
			#print m,feature
			try:
				value = float(feature)
				trackfeatures[n,m] = value
			except ValueError:
				pass
	standardise()
	
def standardise():
	global trackfeatures
	featuretracks = trackfeatures.transpose()
	for i, feature in enumerate(featuretracks):
		mn = mean(feature)
		sd = std(feature)
		for j, value in enumerate(feature):
			x = value
			value = (x - mn) / sd #normalisation formula for mean = 0 SD = 1
			featuretracks[i,j] = value