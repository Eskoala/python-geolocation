from numpy import *
trackfeatures = zeros([4,4])
trackfeatures[0,0:] = 8
trackfeatures[1,0:] = 4 
trackfeatures[2,0:] = 2
trackfeatures[3,0:] = 1
print trackfeatures
multi = zeros([4,4])
n = len(trackfeatures) #number of tracks
m = len(trackfeatures[0]) #number of features per track
last = len(trackfeatures)-1
differences = zeros([n,n,m]) #to hold the difference per track-pair-feature
#which tracks to compare
for firstindex, firstset in enumerate(trackfeatures):
	for secondindex, secondset in enumerate(trackfeatures):
		if firstindex != secondindex:
			diff = zeros([m])
			#compare each feature in turn
			for index, firstvalue in enumerate(firstset):
				secondvalue = secondset[index]
				#print "values are: " + str(firstvalue) +" and "+ str(secondvalue)
				diff[index] = (abs(firstvalue-secondvalue))
				differences[firstindex,secondindex,index] = diff[index] 
	print "The track is "+str(firstindex) + ""
	neighbours = differences[firstindex,:,:]
	print neighbours
	totaldiffs = []
	print totaldiffs
	for index, close in enumerate(neighbours):
		print index
		totaldiffs.append(sum(close))
	if firstindex != 0 & firstindex != last:
		allother = totaldiffs[:firstindex] + totaldiffs[firstindex+1:]
	elif firstindex == 0:
		allother = totaldiffs[1:]
	elif firstindex == last:
		allother = totaldiffs[:-1]
	x = min(allother)
	xi = allother.index(x)
	#print x, xi
	if xi >= firstindex:
		xi = xi+1
	print "The closest track musically is " + str(xi)
	
	
	#else: #it's the final track, so it only appears as a second index
	#	print "we've realised it's the last index"
	#	neighbours = differences[:,firstindex,:]
	#print neighbours

