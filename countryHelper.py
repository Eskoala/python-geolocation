import numpy
import re
from math import *
ncols = 8640
nrows = 3432
xllcorner = -180
yllcorner = -58
llcorner = [-58,-180]
cellsize = 0.0416666666667
NODATA_value = -9999
countries = [118,2]

#gets the population data into a 2d array
def getPopMask():
	with open("glds00ag.txt",'r') as infile:
		for line in infile:
			m = re.match("[A-za-z]",line)
			if not m:
				line = line.strip()
				arr = line.split(' ')
				arr2 = [int(i) for i in arr]
				coords.append(arr2)
	return popgrid

def populationCentroid(arrayOfCoords, popmask):
	poplist = popmask.tolist()
	xArray = []
	yArray = []
	zArray = []
	weights = []
	for pair in arrayOfCoords: #(lat,long)
		lat = radians(pair[0])
		#print lat
		long = radians(pair[1])
		#print long
		x = cos(lat) * cos(long)
		y = cos(lat) * sin(long)
		z = sin(lat)
		xArray.append(x)
		yArray.append(y)
		zArray.append(z)
		rowcol = lltorc(pair[0],pair[1])
		#print pair, rowcol
		weight = popmask[rowcol[0]][rowcol[1]]
		weights.append(weight)
	totweight = numpy.sum(weights)
	weightedx = (numpy.array(xArray) * numpy.array(weights))/totweight
	#print weightedx
	weightedy = (numpy.array(yArray) * numpy.array(weights))/totweight
	weightedz = (numpy.array(zArray) * numpy.array(weights))/totweight
	avx = numpy.mean(weightedx)
	#print avx
	avy = numpy.mean(weightedy)
	#print avy
	avz = numpy.mean(weightedz)
	#print avz
	longitude = atan2(avy,avx)
	#print "long" + str(longitude)
	hypotenuse = sqrt((avx*avx)+(avy*avy))
	latitude = atan2(avz, hypotenuse)
	return [degrees(latitude),degrees(longitude)]

def rctoll(row,col):
	global maxlat
	global minlong
	
	return [nrows-row+minlat,col+minlong] #(0,0) becomes (85,-180)
	
def lltorc(lat,long): #(85,-180) becomes (0,0), (84,-180) becomes (1,0)
	global minlat
	return [nrows-lat+minlat-1,long-minlong-1]
	global minlong
	
def main():
	coords = []
	popmask = getPopMask()
	with open("glbnds.txt",'r') as infile:
		for line in infile:
			m = re.match("[A-za-z]",line)
			if not m:
				line = line.strip()
				arr = line.split(' ')
				arr2 = [int(i) for i in arr]
				coords.append(arr2)
	print "a"
	for country in countries:
		mypoints = set()
		for row, i in enumerate(coords):
			for col, j in enumerate(i):
				if j == country:
					#print country
					degrow = int(round(row/24))
					degcol = int(round(col/24))
					latlong = rctoll(degrow,degcol)
					#get per degree lat longs out of this by /24 and round to int
					mypoints.add(tuple(latlong))
		print mypoints
		#so that we don't have duplicates that confuse the centroid calculation
		uniqlist = list(mypoints)
		print populationCentroid(uniqlist,popmask)
		
main()