tracklines = []
features = []
trackfeatures = []
headers = []
n = 0
m = 0
def getdata(file):
	global tracklines
	features = []
	#the above should be the same length, i.e. each track should have a set of feature-values
	with open(file,'r') as infile:
		for line in infile:
			if "filename" in line:
				tracklines.append(line)
			elif "," in line:
				#print line
				features.append(line)
			else
				headers.append(line)
		if len(tracks) != len(features):
			sys.exit(2)
		n = len(tracks)
		count = 0
	global trackfeatures
	trackfeatures = zeros([len(tracks),len(features[0].split(","))])
	for n, track in enumerate(tracks):
		#print n, track
		featurelist = features[n].split(",")
		m = len(featurelist)
		for m, feature in enumerate(featurelist):
			#print m,feature
			try:
				value = double(feature)
				trackfeatures[n,m] = value
			except ValueError:
				pass # this is the musak one