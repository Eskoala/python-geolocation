require 'csv'
f1rows, f2rows, outfile = [], [], File.open('output.csv', 'wb')
CSV.foreach('Country.txt') do |row| f1rows << {:code => row[0], :name => row[1], :lat => row[2], :long => row[3]} end
CSV.foreach('centroids.csv') do |row| f2rows << {:name => row[0], :lat => row[1], :long => row[2], :population => row[3]} end
CSV.generate('output.csv') do |csv|
f1rows.each do |row|
other_row = f2rows.select{|r| r[:name] == row[:name]}[0]
csv << [row[:code], row[:name], other_row[:lat], other_row[:long]]
end
end
outfile.close