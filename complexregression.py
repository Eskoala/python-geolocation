import geo_helper
from scipy import stats

def llh_to_complex(lat, long, height):
    coords = geo_helper.turn_llh_into_xyz(lat,long,height,'wgs84')
    x=coords[0]
    y=coords[1]
    z=coords[2]
    if(z>=0):
        zetaxi = complex(x,y)/1-z
    else:
        zetaxi = complex(x,-y)/1+z
    return zetaxi

#def regression(data):
print llh_to_complex(52,-4,0)
x = [5.05, 6.75, 3.21, 2.66]
y = [1.65, 26.5, -5.93, 7.96]
print x, y
gradient, intercept, r_value, p_value, std_err = stats.linregress([llh_to_complex(52,-4,0)],[llh_to_complex(52,4,0)])
print "Gradient and intercept", gradient, intercept
print "R-squared", r_value**2
print "p-value", p_value
