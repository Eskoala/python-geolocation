from numpy import *
from numpy.linalg import *
from math import *
import sys

countries = {}
geotracks = {}
tracks = set()
allpairs = []
allsorted = []
n = 0
pairn = 0

def sphericalDistance(a,b):
################
#With thanks to http://www.movable-type.co.uk/scripts/latlong.html
#HAVERSINE in Javascript
#var R = 6371; // km
#var dLat = (lat2-lat1).radians();
#var dLon = (lon2-lon1).radians(); 
#var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
 #       Math.cos(lat1.radians()) * Math.cos(lat2.radians()) * 
 #       Math.sin(dLon/2) * Math.sin(dLon/2); 
#var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
#var d = R * c;
#SPHERICAL LAW OF COSINES IN JAVASCRIPT (Accuracy to ~ 1m)
#var R = 6371; // km
#var d = Math.acos(Math.sin(lat1)*Math.sin(lat2) + 
#                  Math.cos(lat1)*Math.cos(lat2) *
 #                 Math.cos(lon2-lon1)) * R;
	R = 6731 #km
	alat = radians(a[0])
	along = radians(a[1])
	blat = radians(b[0])
	blong = radians(b[1])
	d = acos(sin(alat)*(sin(blat)) + cos(alat)*cos(blat)*cos(blong-along))*R
	return d # max should be 21146 km, min 0

def getlatlong(f):
	global countries
	for line in open(f,'r'):
		parts = line.split(",")
		code = parts[0]
		lat = float(parts[2])
		long = float(parts[3][:-1])
		countries[code] =[lat,long]

def getdata(f):
	global pairn
	global geotracks
	global tracks
	geotrack = []
	for line in open(f,'r'):
		tracks.add(line[:-1]) # not sure yet which will be more use - straight array or hash
		geotracks[line[:-1]] = countries[line[6:8]] # hashed to the lat and long
	n = len(tracks)
	pairn = n/2
	
def makeallnoncolocatedpairs():
	global allpairs
	global tracks
	for t1 in tracks:
		for t2 in tracks:
			#check they aren't from the same country
			if (geotracks[t1] != geotracks[t2]):
				allpairs.append([t1,t2,sphericalDistance(geotracks[t1],geotracks[t2])])

def compare(p1,p2):
	if (p1[2] > p2[2]): 
		return -1
	elif (p1[2]< p2[2]): 
		return 1
	else: return 0
				
def orderbymaxdistance():
	global allpairs
	global allsorted
	allsorted = sorted(allpairs, cmp=compare)

def getvalidset():
	global allsorted
	global pairn
	output = []
	trackset = set()
	for track in tracks:
		trackset.add(track)
	firstpair = allsorted[0]
	output.append(firstpair)
	trackset.remove(firstpair[0])
	trackset.remove(firstpair[1])
	count = 1
	while (len(output) < pairn and count < len(allsorted)):
		pair = allsorted[count]
		count=count+1
		if pair[0] in trackset and pair[1] in trackset:
			output.append(pair)
			print pair[0],pair[1],pair[2]
			trackset.remove(pair[0])
			trackset.remove(pair[1])
			
		
def main():
	songs = "justwavs.txt"
	countries = "Country.txt"
	try:
		getlatlong(countries)
		getdata(songs)
		makeallnoncolocatedpairs()
		orderbymaxdistance()
		print getvalidset()
	except IOError:
		print "cannot open", sys.argv[1]

main()