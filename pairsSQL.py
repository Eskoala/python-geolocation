pairarray = []

def readpairs(f):
	for line in open(f,'r'):
			sparray = line.split(' ')
			sparray[2] = sparray[2][:-1]
			pairarray.append(sparray)
		
def makeSQL():
	start = "INSERT INTO Pair (t1,t2,distance,grp) values ((SELECT id from Track where filename = '"
	middle = "'),(SELECT id from Track where filename = '"
	pairSQL = ""
	count = 0
	grp = -1
	for pair in pairarray:
		if count % 15 == 0:
			grp = grp + 1
		#print pair
		thisSQL = start + pair[0] + middle + pair[1] + "')," + str(pair[2]) + "," + str(grp) + ");"
		pairSQL = pairSQL + thisSQL
		count = count + 1
	return pairSQL

def main():
	pairs = "pairorder.csv"
	try:
		readpairs(pairs)
		print makeSQL()
	except IOError:
		print "cannot open", sys.argv[1]

main()