import numpy
from math import *
import sys
import string
import re
import csv

nrows = 0
ncols = 0
minlat = 0
maxlat = 0
minlong = 0
maxlong = 0
lats = []
longs = []
llcorner = [-58,-180] #temporarily hardcoded
urcorner = []
ulcorner = []
lrcorner = []
countries = []
tracks = []
trackfeatures = []
trackneighbours = [] # each entry is an array of that track's neighbours, index is track index in tracks
k=9
numpy.seterr(invalid = 'raise')
def usage():
	print "specify an arff file as a parameter"

#gets the population data into a 2d array
def getPopMask():
	popgrid = numpy.loadtxt(open("glds10ag60.csv","rb"),delimiter=",",skiprows=6)
	global nrows
	global ncols 
	global lats
	global longs
	global llcorner
	global minlat
	global maxlat
	global minlong
	global maxlong
	
	nrows = popgrid.shape[0]
	ncols = popgrid.shape[1]
	minlat = llcorner[0]
	maxlat = nrows + minlat + 1
	minlong = llcorner[1]
	maxlong = ncols + minlong + 1
	lats = range(minlat,maxlat)
	longs = range(minlong,maxlong)
	return popgrid

#X=(z-mean)/standarddeviation so mean is 0 and sd is 1
#NEW WAY: make a number between 0 and 1 by dividing by the maximum number.
def standardisePopMask(popMask):
	stdpopmask = numpy.array((nrows,ncols))
	maxvalue = numpy.max(popMask)
	poplist = popMask.tolist()
	#print poplist
	poplist = [[i if i != -9999 else 0 for i in x] for x in poplist] #swap -9999 for 0
	#print poplist
	#print maxvalue
	popMask = numpy.array(poplist)
	#print popMask
	#mn = popMask.mean(dtype=numpy.float64)
	#print mn
	#sd = popMask.std(dtype=numpy.float64)
	#print sd
	
	stdpopmask = [[(i/maxvalue) for i in x] for x in popMask] #divide everything by the maximum
	return numpy.array(stdpopmask)
	
def getdata(file):
	#print "getting data"
	global tracks
	features = []
	#the above should be the same length, i.e. each track should have a set of feature-values
	with open(file,'r') as infile:
		for line in infile:
			if "filename" in line:
				index = line.rfind('/')
				trackname = line[(index+1):]
				#print trackname
				tracks.append(trackname)
			elif "," in line:
				#print line
				features.append(line)
		if len(tracks) != len(features):
			sys.exit(2)
	global trackfeatures
	trackfeatures = numpy.zeros([len(tracks),len(features[0].split(","))-1])
	for n, track in enumerate(tracks):
		#print n, track
		featurelist = features[n].split(",")
		for m, feature in enumerate(featurelist):
			#print m,feature
			try:
				value = float(feature)
				trackfeatures[n,m] = value
			except ValueError:
				pass
	standardise()
	#print n,m

#X=(z-mean)/standarddeviation
def standardise():
	global trackfeatures
	#print trackfeatures[0]
	#print trackfeatures.shape
	featuretracks = trackfeatures.transpose()
	#print featuretracks[0]
	#print featuretracks.shape
	for i, feature in enumerate(featuretracks):
		mn = numpy.mean(feature)
		sd = numpy.std(feature)
		#print mn,sd
		for j, value in enumerate(feature):
			x = value
			#print x
			value = (x - mn) / sd
			#print value
			featuretracks[i,j] = value
	#tf = featuretracks.transpose()
	#print trackfeatures[0]
	
def getlatlong(thefile):
	global countries
	#for line in open(thefile,'r'):
	#	bits = line.split(",")
	csvin = csv.reader(open(thefile, 'rb'),delimiter=',',quotechar='|')
	for row in csvin: 	
		code = row[0]
		lat = float(row[2])
		long = float(row[3][:-1])
		countries.append([code,lat,long])
	#print countries
	
"""
With thanks to http://www.movable-type.co.uk/scripts/latlong.html
HAVERSINE in Javascript
var R = 6371; // km
var dLat = (lat2-lat1).radians();
var dLon = (lon2-lon1).radians(); 
var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(lat1.radians()) * Math.cos(lat2.radians()) * 
        Math.sin(dLon/2) * Math.sin(dLon/2); 
var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
var d = R * c;
SPHERICAL LAW OF COSINES IN JAVASCRIPT (Accuracy to ~ 1m)
var R = 6371; // km
var d = Math.acos(Math.sin(lat1)*Math.sin(lat2) + 
                  Math.cos(lat1)*Math.cos(lat2) *
                  Math.cos(lon2-lon1)) * R;
"""
def sphericalDistance(true,musical):
	R = 6731 #km
	truelat = radians(true[0])
	truelong = radians(true[1])
	muslat = radians(musical[0])
	muslong = radians(musical[1])
	d = acos(sin(truelat)*(sin(muslat)) + cos(truelat)*cos(muslat)*cos(muslong-truelong))*R
	return d # max should be 21146 km, min 0

"""
Geodesic midpoint                          

with thanks to http://www.geomidpoint.com/calculation.html
Given the values for the first location in the list:
Lat1, lon1
Convert lat/lon to Cartesian coordinates for first location.
X1 = cos(lat1) * cos(lon1)
Y1 = cos(lat1) * sin(lon1)
Z1 = sin(lat1)
Repeat for all remaining locations in the list.
take average x,y,z
Convert average x, y, z coordinate to latitude and longitude.
Lon = atan2(y, x)
Hyp = sqrt(x * x + y * y)
Lat = atan2(z, hyp)
Special case:
If abs(x) < 10-9 and abs(y) < 10-9 and abs(z) < 10-9 then the geographic midpoint is the center of the earth.
"""
def avgSphericalCoords(arrayOfCoords):

	xArray = []
	yArray = []
	zArray = []
	for pair in arrayOfCoords: #(lat,long)
		lat = radians(pair[0])
		#print lat
		long = radians(pair[1])
		#print long
		x = cos(lat) * cos(long)
		y = cos(lat) * sin(long)
		z = sin(lat)
		xArray.append(x)
		yArray.append(y)
		zArray.append(z)
	avx = numpy.average(xArray)
	avy = numpy.average(yArray)
	avz = numpy.average(zArray)
	#print avx, avy, avz
	longitude = atan2(avy,avx)
	#print "long" + str(longitude)
	hypotenuse = sqrt((avx*avx)+(avy*avy))
	latitude = atan2(avz, hypotenuse)
	return [degrees(latitude),degrees(longitude)]
	
"""
with thanks to http://www.geomidpoint.com/calculation.html
Given the values for the first location in the list:
Lat1, lon1
Convert Lat1 and Lon1 from degrees to radians.
lat1 = lat1 * PI/180
lon1 = lon1 * PI/180
Convert lat/lon to Cartesian coordinates for first location.
X1 = cos(lat1) * cos(lon1)
Y1 = cos(lat1) * sin(lon1)
Z1 = sin(lat1)
Find weight from population data for lat/long
Repeat steps 1sta-3 for all remaining locations in the list.
Compute combined total weight for all locations.
Totweight = w1 + w2 + ... + wn
Compute weighted average x, y and z coordinates.
x = ((x1 * w1) + (x2 * w2) + ... + (xn * wn)) / totweight
y = ((y1 * w1) + (y2 * w2) + ... + (yn * wn)) / totweight
z = ((z1 * w1) + (z2 * w2) + ... + (zn * wn)) / totweight
Convert average x, y, z coordinate to latitude and longitude. Note that in Excel and possibly some other applications, the parameters need to be reversed in the atan2 function, for example, use atan2(X,Y) instead of atan2(Y,X).
Lon = atan2(y, x)
Hyp = sqrt(x * x + y * y)
Lat = atan2(z, hyp)
Convert lat and lon to degrees.
lat = lat * 180/PI
lon = lon * 180/PI
Special case:
If abs(x) < 10-9 and abs(y) < 10-9 and abs(z) < 10-9 then the geographic midpoint is the center of the earth.
"""
def populationCentroid(arrayOfCoords, popmask):
	poplist = popmask.tolist()
	xArray = []
	yArray = []
	zArray = []
	weights = []
	for pair in arrayOfCoords: #(lat,long)
		lat = radians(pair[0])
		#print lat
		long = radians(pair[1])
		#print long
		x = cos(lat) * cos(long)
		y = cos(lat) * sin(long)
		z = sin(lat)
		xArray.append(x)
		yArray.append(y)
		zArray.append(z)
		rowcol = lltorc(pair[0],pair[1])
		weight = popmask[rowcol[0]][rowcol[1]]
		weights.append(weight)
	totweight = numpy.sum(weights)
	if(totweight ==0):
		#print arrayOfCoords
		weightedx = [x]
		weightedy = [y]
		weightedz = [z]
	else:
		weightedx = (numpy.array(xArray) * numpy.array(weights))/totweight
		#print weightedx
		weightedy = (numpy.array(yArray) * numpy.array(weights))/totweight
		weightedz = (numpy.array(zArray) * numpy.array(weights))/totweight
	avx = numpy.mean(weightedx)
	#print avx
	avy = numpy.mean(weightedy)
	#print avy
	avz = numpy.mean(weightedz)
	#print avz
	longitude = atan2(avy,avx)
	#print "long" + str(longitude)
	hypotenuse = sqrt((avx*avx)+(avy*avy))
	latitude = atan2(avz, hypotenuse)
	return [degrees(latitude),degrees(longitude)]

#returns an array of lat long pairs that are within radius
def getNeighbourhood(neighbours, midpoint):
	#first find the radius
	furthestaway = []
	radius = 0
	for n in neighbours:
		ndist = sphericalDistance(n,midpoint)
		if ndist >= radius:
			radius = ndist
			furthestaway = n
	return [midpoint,radius]

#now find all the lat longs that are within this radius
def getPointsInNeighbourhood(midpoint,radius):
	global lats
	global longs
	
	inPoints = []
	if (radius == 0): #no point doing the for loop
		inPoints.append(midpoint)
		return inPoints
	for x in lats:
		for y in longs:
			if sphericalDistance([x,y],midpoint) < radius:
				inPoints.append([x,y])
	#a circle of the points within the R of the most distant N
	if not inPoints:
		inPoints.append(midpoint)
	return inPoints

#WORKS
def rctoll(row,col):
	global maxlat
	global minlong
	
	return [nrows-row+minlat,col+minlong] #(0,0) becomes (85,-180)

def lltorc(lat,long): #(85,-180) becomes (0,0), (84,-180) becomes (1,0)
	global minlat
	global minlong
	return [nrows-lat+minlat-1,long-minlong-1]

def knnPop(stdpopmask):
	alld = []
	alldp = []
	n = len(trackfeatures) #number of tracks
	m = len(trackfeatures[0]) #number of features per track
	last = len(trackfeatures)-1
	differences = numpy.zeros([n,n,m], dtype=numpy.float32) #to hold the difference per track-pair-feature
	for firstindex, firstset in enumerate(trackfeatures): # is suboptimal but duplication allows easy indexing
		for secondindex, secondset in enumerate(trackfeatures):
			if firstindex != secondindex:
				diff = numpy.zeros([m])
				#compare each feature in turn
				for index, firstvalue in enumerate(firstset):
					secondvalue = secondset[index]
					#print "values are: " + str(firstvalue) +" and "+ str(secondvalue)
					diff[index] = numpy.square(firstvalue-secondvalue)
					differences[firstindex,secondindex,index] = diff[index] 
		neighbours = differences[firstindex,:,:]
		#print neighbours
		totaldiffs = []
		#print totaldiffs
		for index, close in enumerate(neighbours):
			#print index
			totaldiffs.append(sqrt(sum(close)))
		if firstindex != 0 & firstindex != last:
			allother = totaldiffs[:firstindex] + totaldiffs[firstindex+1:]
		elif firstindex == 0: #first one has no preceding indices
			allother = totaldiffs[1:]
		elif firstindex == last: #last has no succeeding indices
			allother = totaldiffs[:-1]
		#print str(k)
		if k == 1:
			#print "k = 1"
			x = min(allother)
			xi = allother.index(x)
			if xi >= firstindex: #rejig indices after removal of self
				xi = xi+1
			#print tracks[firstindex] + "is closest to " + tracks[xi] 
			realCoords = [0,0]
			nearestCoords = []
			done = 0
			for country in countries:
				if country[0] == tracks[firstindex][:2]:
					realCoords = [country[1],country[2]]
					done = done+1
				if country[0] == tracks[xi][:2]:
					nearestCoords = [country[1],country[2]]
					done = done+1
				if done == 2: #stop when both found
					break
			#print realCoords, nearestCoords
			d = sphericalDistance(realCoords,nearestCoords)
			print tracks[firstindex][:2] + "," + str(d) + "," + str(nearestCoords[0]) + "," + str(nearestCoords[1])
			alld.append(d)
		else:
			#print "k != 1"
			findmin = []
			for item in allother:
				findmin.append(item)
			neighbours = []
			nindices = []
			#print tracks[firstindex][:-5]
			for i in range(k):
				closest = min(findmin)
				closesti = allother.index(closest)
				if closesti >= firstindex: #rejig indices after removal of self
					closesti = closesti+1
				#print closest, closesti, tracks[closesti]
				
				
				#print tracks[closesti][:4] + " "
				neighbours.append(closest)
				nindices.append(closesti)
				findmin.remove(closest)
			realCoords = [0,0]
			nearestCoords = []
			done = 0
			for country in countries:
			#CHANGING BRIEFLY
				if country[0] == tracks[firstindex][:2]:
				#if country[0] == tracks[firstindex][3:5]:
					realCoords = [country[1],country[2]]
					done = done+1
				for nn in nindices:
					if country[0] == tracks[nn][:2]:
					#if country[0] == tracks[nn][3:5]:
						nearestCoords.append([country[1],country[2]])
						done = done+1
				if done == (1+k): #stop when both found
					break
			#print realCoords, nearestCoords
			average = avgSphericalCoords(nearestCoords)
			#print realCoords, average	
			midAndRadius = getNeighbourhood(nearestCoords,average)
			points = getPointsInNeighbourhood(midAndRadius[0], midAndRadius[1])
			centroid = populationCentroid(points,stdpopmask)
			#print centroid
			d = sphericalDistance(realCoords, average)
			dp = sphericalDistance(realCoords,centroid)
			#if isnan(dp):st
			#	print centroid
			print tracks[firstindex][:2] + "," + str(d) + "," + str(dp)
			alld.append(d)
			alldp.append(dp)
	#print alld
	#
	maxi = max(alld)
	mini = min(alld)
	tot = 0
	for d in alld:
		tot = tot + d
	avg = tot/len(alld)
	maxp = max(alldp)
	minp = min(alldp)
	tot = 0
	for d in alldp:
		tot = tot + d
	avgp = tot/len(alldp)
	print maxi,mini,avg,maxp,minp,avgp


	
def main():
	infile = "NMdef.arff"
	countryfile = "countrycentroids.csv"
	try:
		global k
		#print "lltorc"
		#print lltorc(0,0)
		#print lltorc(1,1)
		#print lltorc(2,2)
		#print lltorc(85,-180)
		#print lltorc(85,180)
		#print lltorc(-58,-180)
		#print lltorc(-58,180)
		#print "rctoll"
		#print rctoll(0,0)
		#print rctoll(1,1)
		#print rctoll(2,2)
		#print rctoll(0,359)
		#print rctoll(142,0)
		#print rctoll(142,359)
		#print rctoll(85,180)
		getdata(infile)
		getlatlong(countryfile)
		rawpopmask = getPopMask()
		normpopmask = standardisePopMask(rawpopmask)
		knnPop(normpopmask)
	#except IndexError:
		#usage()
	except IOError:
		print "cannot open", infile, countryfile, 

main()
